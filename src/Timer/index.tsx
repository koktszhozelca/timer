import React, { PureComponent } from 'react';
import './index.css';
import moment from 'moment';
import { EventEmitter } from 'events';
import Speech from '../Speech';
import Help from '../Help';

const THRESHOLD = 500;
const TimerMode = ["Time Display", "Stopwatch", "Set Alarm Mode", "Set Time Mode", "Back To Tutor"];

enum TIMER_MAIN_STATE {
    TimeDisplay, StopWatch, SetAlarmMode, SetTimeMode, BackToTutor
}

enum TIMER_SUB_STATE {
    Start, Hour, Minute, Second
}

enum PRESS_MODE {
    SHORT, LONG
}

interface State {
    mainState: TIMER_MAIN_STATE,
    subState: TIMER_SUB_STATE,
    hour: string,
    minute: string,
    second: string,
    isNormalMode: boolean
}

class Timer extends PureComponent<any, {}> {
    public state: State = {
        mainState: TIMER_MAIN_STATE.TimeDisplay,
        subState: TIMER_SUB_STATE.Start,
        hour: "00",
        minute: "00",
        second: "00",
        isNormalMode: true
    }
    private startPressTime: any = null;

    private hour: React.RefObject<HTMLSpanElement> = React.createRef();
    private minute: React.RefObject<HTMLSpanElement> = React.createRef();
    private second: React.RefObject<HTMLSpanElement> = React.createRef();

    private help: React.RefObject<HTMLDivElement> = React.createRef();
    private helpForm: React.RefObject<Help> = React.createRef();

    private eventHandler: EventEmitter;

    constructor(props: any) {
        super(props);

        //Setup the event handler
        this.eventHandler = new EventEmitter();
        this.eventHandler.on("interrupt", () => {
            this.setState({ subState: TIMER_SUB_STATE.Start },
                () => this.dismissTimeFunction());
        });
        setTimeout(() => {
            Speech.speak("Watch application is opened.");
            Speech.speak("Time display mode is active.");
        }, 1000)
    }

    /*
        [OnPressHandler]
        Record the start pressing time.
    */
    onPressDown = () => {
        this.startPressTime = moment();
    }

    /*
        [OnPressHandler]
        Calculate the duration, and determine the press mode.
        The eventHandler is a async event that 
        responsible for firing an interrupt to exit the set-time-mode. 
        The eventHandler sends the interript if and only if the main 
        state is either SetAlarmMode / SetTimeMode.
    */
    onPressRelease = () => {
        let diff = moment().diff(this.startPressTime);
        let duration = moment.duration(diff).asMilliseconds();
        let pressMode: PRESS_MODE = this.interpret(duration);
        this.mainStateRouter(pressMode);
        if (pressMode === PRESS_MODE.LONG &&
            this.state.mainState >= TIMER_MAIN_STATE.SetAlarmMode)
            this.eventHandler.emit("interrupt");
    }

    /*
        A function to check whether the press duration 
        is longer than the threshold (500 ms).
    */
    interpret = (duration: number) => {
        if (duration >= THRESHOLD) {
            navigator.vibrate([20]);
            return PRESS_MODE.LONG;
        }
        else {
            navigator.vibrate([10]);
            return PRESS_MODE.SHORT;
        }
    }

    /*
        Serve the corresponding functions based on the
        press mode.
    */
    mainStateRouter = async (pressMode: PRESS_MODE) => {
        switch (pressMode) {
            case PRESS_MODE.LONG:
                return await this.nextMainState();
            case PRESS_MODE.SHORT:
                await this.subStateRouter();
                await this.toggleStopWatch();
            default: return; //It does not exist.
        }
    }

    /*
        Route to set timeMode if and only if
        the main state is set-time-mode / set-alarm-mode
    */
    subStateRouter = async () => {
        switch (this.state.mainState) {
            case TIMER_MAIN_STATE.SetTimeMode:
            case TIMER_MAIN_STATE.SetAlarmMode:
                return await this.nextSubState();
            case TIMER_MAIN_STATE.BackToTutor:
                return await this.navigate();
            default: return; //It does not exist.
        }
    }

    /*
        Switching the mode between time display and stopwatch.
    */
    toggleStopWatch = async () => {
        if (this.state.mainState > TIMER_MAIN_STATE.StopWatch) return;
        return await this.setState({
            mainState:
                this.state.mainState === TIMER_MAIN_STATE.TimeDisplay ?
                    TIMER_MAIN_STATE.StopWatch : TIMER_MAIN_STATE.TimeDisplay
        }, () => {
            this.state.mainState === TIMER_MAIN_STATE.TimeDisplay ?
                Speech.speak("Time display mode is active.") :
                Speech.speak("Stopwatch mode is active.")
        })
    }

    //Main-STN
    nextMainState = async () => {
        switch (this.state.mainState) {
            case TIMER_MAIN_STATE.TimeDisplay:
                return await this.setState({ mainState: TIMER_MAIN_STATE.StopWatch }, () => Speech.speak("Stopwatch mode is active."))
            case TIMER_MAIN_STATE.StopWatch:
                return await this.setState({ mainState: TIMER_MAIN_STATE.SetAlarmMode }, () => Speech.speak("Set alarm mode is active, short press to start."))
            case TIMER_MAIN_STATE.SetAlarmMode:
                return await this.setState({ mainState: TIMER_MAIN_STATE.SetTimeMode }, () => Speech.speak("Set time mode is active, short press to start."))
            case TIMER_MAIN_STATE.SetTimeMode:
                return await this.setState({ mainState: TIMER_MAIN_STATE.BackToTutor }, () => {
                    Speech.speak("Short press if you want to navigate to the tutor application.")
                    this.setSpecialMode()
                })
            case TIMER_MAIN_STATE.BackToTutor:
                this.setNormalMode();
                return await this.setState({ mainState: TIMER_MAIN_STATE.TimeDisplay })
            default: return; //It does not exist.
        }
    }

    //Sub-STN
    nextSubState = async () => {
        switch (this.state.subState) {
            case TIMER_SUB_STATE.Start:
                Speech.speak("Configure hours.")
                return await this.setState({ subState: TIMER_SUB_STATE.Hour }, () => this.setHour());
            case TIMER_SUB_STATE.Hour:
                Speech.speak("Configure minute.")
                return await this.setState({ subState: TIMER_SUB_STATE.Minute }, () => this.setMinute());
            case TIMER_SUB_STATE.Minute:
                Speech.speak("Configure second.")
                return await this.setState({ subState: TIMER_SUB_STATE.Second }, () => this.setSecond());
            case TIMER_SUB_STATE.Second:
                Speech.speak("Please check the time.")
                return await this.setState({ subState: TIMER_SUB_STATE.Start },
                    () => this.dismissTimeFunction());
            default: return; //It does not exist.
        }
    }

    navigate = () => {
        window.location.href = "http://www4.comp.polyu.edu.hk/~15011089d/COMP2222/tutor"
    }

    //Related to UIs
    dismissTimeFunction = () => {
        this.hour.current!.classList.remove("flashing");
        this.minute.current!.classList.remove("flashing");
        this.second.current!.classList.remove("flashing");
    }

    setHour = () => {
        this.hour.current!.classList.add("flashing");
        this.minute.current!.classList.remove("flashing");
        this.second.current!.classList.remove("flashing");
    }

    setMinute = () => {
        this.hour.current!.classList.remove("flashing");
        this.minute.current!.classList.add("flashing");
        this.second.current!.classList.remove("flashing");
    }

    setSecond = () => {
        this.hour.current!.classList.remove("flashing");
        this.minute.current!.classList.remove("flashing");
        this.second.current!.classList.add("flashing");
    }

    setSpecialMode = () => {
        this.hour.current!.style.fontSize = "5vw";
        this.minute.current!.style.fontSize = "5vw";
        this.second.current!.style.fontSize = "5vw";
        this.setState({ hour: "", second: "", minute: "(Short press)", isNormalMode: false })
    }

    setNormalMode = () => {
        this.hour.current!.style.fontSize = "10vw";
        this.minute.current!.style.fontSize = "10vw";
        this.second.current!.style.fontSize = "10vw";
        this.setState({ hour: "00", second: "00", minute: "00", isNormalMode: true })
    }

    resize = () => {
        this.forceUpdate();
    }

    componentDidMount() {
        window.addEventListener("resize", this.resize)
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.resize)
    }

    showHelp = () => {
        navigator.vibrate([10]);
        this.helpForm.current!.setState({ title: "Stopwatch", message: "Long press (2 seconds) to switch mode, Short press to call the function." }, () => {
            this.helpForm.current!.show();
        })
    }

    hideHelp = () => {        
        this.helpForm.current!.hide();
    }

    render() {
        return (
            <div className="timer-container">
                <Help ref={this.helpForm} handler={this} />
                <div className="timer-background">
                    <div className="timer-background-frame">
                        <div className="timer-display">
                            <div className="timer-mode">
                                <div className="timer-help" onClick={this.showHelp}>
                                    <span className="material-icons timer-help-icon">contact_support</span>
                                    <span className="timer-help-text">How it works?</span>
                                </div>
                                <hr />
                                {TimerMode[this.state.mainState]}
                                <hr />
                            </div>
                            <div className="timer-time">
                                {
                                    <div>
                                        <span ref={this.hour}>{this.state.hour}</span>
                                        {this.state.isNormalMode ? ":" : ""}
                                        <span ref={this.minute}>{this.state.minute}</span>
                                        {this.state.isNormalMode ? ":" : ""}
                                        <span ref={this.second}>{this.state.second}</span>
                                    </div>
                                }
                            </div>
                            {
                                window.innerWidth <= 420 ?
                                    <div className="timer-button"
                                        onTouchStart={this.onPressDown}
                                        onTouchEnd={this.onPressRelease}></div>
                                    :
                                    <div className="timer-button"
                                        onMouseDown={this.onPressDown}
                                        onMouseUp={this.onPressRelease}></div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Timer;