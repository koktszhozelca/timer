class Speech {
    static speak(message: string) {
        // Create a new instance of SpeechSynthesisUtterance.
        var msg = new SpeechSynthesisUtterance();

        // Set the text.
        msg.text = message;

        // Set the attributes.
        msg.volume = 1;
        msg.rate = 1;
        msg.pitch = 1;

        // If a voice has been selected, find the voice and set the
        // utterance instance's voice attribute.

        msg.voice = speechSynthesis.getVoices()[0];

        // Queue this utterance.
        window.speechSynthesis.speak(msg);
    }
}

export default Speech;